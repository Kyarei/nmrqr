use ghakuf::messages::*;

use crate::generation::utils::chord::*;
use crate::generation::utils::*;

pub const TEMPO: u32 = 120;

pub fn set_tempo(tempo: u32) -> Message {
    let midi_tempo: u32 = 60 * 1000000 / tempo;
    Message::MetaEvent {
        delta_time: 0,
        event: MetaEvent::SetTempo,
        data: [
            (midi_tempo >> 16) as u8,
            (midi_tempo >> 8) as u8,
            midi_tempo as u8,
        ]
        .to_vec(),
    }
}

pub fn set_time_signature(num: u8, den: u8) -> Message {
    Message::MetaEvent {
        delta_time: 0,
        event: MetaEvent::TimeSignature,
        data: [num, den, 24, 8].to_vec(),
    }
}

pub fn end_of_track() -> Message {
    Message::MetaEvent {
        delta_time: 0,
        event: MetaEvent::EndOfTrack,
        data: Vec::new(),
    }
}

pub fn add_note(list: &mut Vec<Message>, duration: u32, pitch: u8, delay: u32) {
    list.push(Message::MidiEvent {
        delta_time: delay,
        event: MidiEvent::NoteOn {
            ch: 0,
            note: pitch,
            velocity: 0x7f,
        },
    });
    list.push(Message::MidiEvent {
        delta_time: duration,
        event: MidiEvent::NoteOff {
            ch: 0,
            note: pitch,
            velocity: 0x7f,
        },
    });
}

pub fn write_voice(list: &mut Vec<Message>, voice: &Voice) {
    list.push(Message::TrackChange);
    let mut rest_duration: u32 = 0;
    let last_note_index = voice.notes.len() - 1;
    for (index, (note_start, note_pitch)) in voice.notes.iter().enumerate() {
        let is_last_note = index == last_note_index;
        let note_end = if is_last_note {
            voice.total_duration
        } else {
            voice.notes[index + 1].0
        };
        match note_pitch {
            Some(pitch) => {
                add_note(list, note_end - note_start, *pitch, rest_duration);
                rest_duration = 0;
            }
            None => {
                rest_duration += note_end - note_start;
            }
        }
    }
    list.push(end_of_track());
}

pub fn write_tune(list: &mut Vec<Message>, tune: &Tune) {
    for v in tune.voices.iter() {
        write_voice(list, v);
    }
}

pub fn write_progression(list: &mut Vec<Message>, progression: &ChordProgression) {
    list.push(Message::TrackChange);
    for chord in progression.chords.iter() {
        for pitch in chord.notes.iter() {
            list.push(Message::MidiEvent {
                delta_time: 0,
                event: MidiEvent::NoteOn {
                    ch: 0,
                    note: *pitch,
                    velocity: 0x7f,
                },
            });
        }
        for (i, pitch) in chord.notes.iter().enumerate() {
            list.push(Message::MidiEvent {
                delta_time: if i == 0 { 4 * QUARTER_NOTE } else { 0 },
                event: MidiEvent::NoteOff {
                    ch: 0,
                    note: *pitch,
                    velocity: 0x7f,
                },
            });
        }
    }
    list.push(end_of_track());
}
