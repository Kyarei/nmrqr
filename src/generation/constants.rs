use crate::generation::utils::*;

pub const SONG_LEN: u32 = 8 * BAR_44;

// Things to tweak
pub const CHROMATIC_NOTE_PENALTY: f64 = 3.0;
pub const CONSECUTIVE_CHROMATIC_NOTE_PENALTY: f64 = 3.0;
pub const PENTATONIC_NOTE_BONUS: f64 = 0.5;
pub const REST_PENALTY_PER_EIGHTH_NOTE: f64 = 1.5;
pub const LARGE_JUMP_PENALTY_MULTIPLIER: f64 = 1.0;
pub const TRITONE_PENALTY: f64 = 20.0;
pub const SAME_NOTE_PENALTY: f64 = 0.8;
pub const DISSONANCE_PENALTY_MULTIPLIER: f64 = 3.5;
pub const DOUBLED_NOTE_PENALTY: f64 = 10.0;
pub const FULL_CHORD_BONUS: f64 = 10.0;

pub const EVOLVE_ITERS: u32 = 100_000;
