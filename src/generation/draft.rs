use rand::prelude::*;
use std::cmp::{max, min};

use crate::generation::constants::*;
use crate::generation::utils::chord::*;
use crate::generation::utils::*;
use std::iter::repeat_with;

pub fn note_duration(s: &mut impl Rng) -> u32 {
    // Generate either a quarter note or an eighth note
    if s.gen::<u8>() % 2 == 0 {
        QUARTER_NOTE
    } else {
        EIGHTH_NOTE
    }
}

pub fn generate_voice(s: &mut impl Rng, starting_pitch: u8) -> Voice {
    let mut notes: Vec<(u32, Option<u8>)> = Vec::new();
    let mut total_duration: u32 = 0;
    let mut current_pitch = starting_pitch;
    while total_duration < SONG_LEN {
        let duration = note_duration(s);
        let duration = min(duration, SONG_LEN - total_duration);
        notes.push((total_duration, Some(current_pitch)));
        current_pitch = loop {
            let cpi32 = current_pitch as i32;
            let spi32 = starting_pitch as i32;
            let next_pitch = cpi32 + note_jump(s) as i32;
            // Prevent going out of range
            let next_pitch = max(0, min(127, next_pitch));
            // If the pitch hasn't strayed too far, then exit;
            // otherwise, retry
            if next_pitch >= spi32 - 7 && next_pitch <= spi32 + 7 {
                break next_pitch;
            }
        } as u8;
        total_duration += duration;
    }
    Voice {
        notes,
        total_duration,
        pinned: false,
    }
}

pub fn generate_tune(s: &mut impl Rng, starting_pitches: &[u8], prog: &ChordProgression) -> Tune {
    let mut voices = Vec::<Voice>::new();
    for (_, p) in starting_pitches.iter().enumerate() {
        voices.push(generate_voice(s, *p));
    }
    voices.push(progression_to_voice(&prog));
    Tune { voices }
}

pub fn generate_random_chord(s: &mut impl Rng) -> Chord {
    let mut notes: [u8; NOTES_PER_CHORD] = [0; NOTES_PER_CHORD];
    for i in 0..NOTES_PER_CHORD {
        notes[i] = loop {
            let k = s.gen_range(36, 60);
            if !notes[0..i].contains(&k) {
                break k;
            }
        };
    }
    notes.sort_unstable();
    Chord { notes }
}

pub fn generate_random_progression(s: &mut impl Rng, count: usize) -> ChordProgression {
    ChordProgression {
        chords: repeat_with(|| generate_random_chord(s))
            .take(count)
            .collect(),
    }
}

const ALBERTI: &[(u32, Option<u8>)] = &[
    (0 * EIGHTH_NOTE, Some(0)),
    (1 * EIGHTH_NOTE, Some(2)),
    (2 * EIGHTH_NOTE, Some(1)),
    (3 * EIGHTH_NOTE, Some(2)),
    (4 * EIGHTH_NOTE, Some(0)),
    (5 * EIGHTH_NOTE, Some(2)),
    (6 * EIGHTH_NOTE, Some(1)),
    (7 * EIGHTH_NOTE, Some(2)),
];

pub fn progression_to_voice(prog: &ChordProgression) -> Voice {
    let mut notes: Vec<(u32, Option<u8>)> = Vec::new();
    for (i, chord) in prog.chords.iter().enumerate() {
        let offset = BAR_44 * (i as u32);
        for n in ALBERTI {
            let new_note = match n {
                (time, Some(s)) => (time + offset, Some(chord.notes[*s as usize])),
                (time, None) => (time + offset, None),
            };
            notes.push(new_note);
        }
    }
    Voice {
        notes,
        total_duration: BAR_44 * (prog.chords.len() as u32),
        pinned: true,
    }
}
