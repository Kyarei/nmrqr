

pub const NOTES_PER_CHORD: usize = 4;

#[derive(Copy, Clone, Debug)]
pub struct Chord {
    pub notes: [u8; NOTES_PER_CHORD],
}

#[derive(Clone, Debug)]
pub struct ChordProgression {
    pub chords: Vec<Chord>,
}
