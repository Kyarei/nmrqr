use rand::prelude::*;

pub mod chord;

pub const EIGHTH_NOTE: u32 = 240;
pub const QUARTER_NOTE: u32 = 2 * EIGHTH_NOTE;
pub const BAR_44: u32 = QUARTER_NOTE * 4;

pub const INTERVAL_DISSONANCE_BY_SEMITONES: [f64; 12] = [
    0.0, 10.0, 3.0, -0.2, -0.5, -2.0, 5.0, -2.0, -0.5, -0.2, 3.0, 10.0,
];

lazy_static::lazy_static! {
    pub static ref CHORD_DISSONANCE: [f64; (1 << 12)] = {
        let mut table: [f64; (1 << 12)] = [0.0; (1 << 12)];
        for i in 0..(1 << 12) {
            let mut total_dissonance = 0.0;
            for p1 in 0..12 {
                if (i & (1 << p1)) == 0 { continue; }
                for p2 in 0..p1 {
                    if (i & (1 << p2)) == 0 { continue; }
                    let diff = p1 - p2;
                    total_dissonance += INTERVAL_DISSONANCE_BY_SEMITONES[diff];
                }
            }
            table[i] = total_dissonance;
        }
        table
    };
    pub static ref COMPLETES_FULL_CHORDS: [bool; (1 << 12)] = {
        let mut table: [bool; (1 << 12)] = [false; (1 << 12)];
        for i in 0..(1 << 12) {
            for shift in 0..12 {
                let rotated_chord = (i >> shift) | (i << (12 - shift));
                if (rotated_chord & MAJOR_CHORD_BITMASK) == MAJOR_CHORD_BITMASK
                    || (rotated_chord & MINOR_CHORD_BITMASK) == MINOR_CHORD_BITMASK
                {
                    table[i as usize] = true;
                    break;
                }
            }
        }
        table
    };
}

pub const MAJOR_CHORD_BITMASK: u32 = 0b10010001;
pub const MINOR_CHORD_BITMASK: u32 = 0b10001001;

pub fn to_note(n: &(u32, Option<u8>)) -> Option<(u32, u8)> {
    match n {
        (t, Some(p)) => Some((*t, *p)),
        _ => None,
    }
}

pub fn is_note(n: &(u32, Option<u8>)) -> bool {
    return to_note(n).is_some();
}

#[derive(Clone)]
pub struct Voice {
    pub notes: Vec<(u32, Option<u8>)>, // (time, pitch), ordered by increasing time
    pub total_duration: u32,
    pub pinned: bool,
}

#[derive(Clone)]
pub struct Tune {
    pub voices: Vec<Voice>,
}

pub fn note_jump(s: &mut impl Rng) -> i8 {
    // Favour steps over leaps:
    // change      | weight
    // 0           | 4
    // +/- 1, 2    | 8
    // +/- 3, 4    | 4
    // +/- 5, 7, 8 | 2
    // +/- 9, 12   | 1
    // total       | 36
    match s.gen_range(0, 68) {
        0..4 => 0,
        4..12 => 1,
        12..20 => -1,
        20..28 => 2,
        28..36 => -2,
        36..40 => 3,
        40..44 => -3,
        44..48 => 4,
        48..52 => -4,
        52 | 53 => 5,
        54 | 55 => 7,
        56 | 57 => 8,
        58 | 59 => -5,
        60 | 61 => -7,
        62 | 63 => -8,
        64 => 9,
        65 => 12,
        66 => -9,
        67 => -12,
        n => panic!("value outside 0..68 returned: {}", n),
    }
}

pub fn is_in_c_major(pitch: u8) -> bool {
    let p = pitch % 12;
    [
        true, false, true, false, true, true, false, true, false, true, false, true,
    ][p as usize]
}

pub fn is_in_c_major_pentatonic(pitch: u8) -> bool {
    let p = pitch % 12;
    [
        true, false, true, false, true, false, false, true, false, true, false, false,
    ][p as usize]
}

pub fn interval_dissonance(diff: i32) -> f64 {
    let diff = diff.abs() % 12;
    INTERVAL_DISSONANCE_BY_SEMITONES[diff as usize]
}
