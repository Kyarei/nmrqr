use adjacent_pair_iterator::AdjacentPairIterator;
use rand::prelude::*;
use std::cmp::{max, min};

use crate::generation::constants::*;
use crate::generation::utils::chord::*;
use crate::generation::utils::*;

pub fn evaluate_voice(voice: &Voice) -> f64 {
    // penalty for each note outside C major, plus additional penalties
    // for consecutive chromatic notes
    // small bonus for notes in C pentatonic
    // penalty for each eighth note of rest
    let mut p = 0.0;
    let mut consecutive_chromatic_notes = 0;
    for note in &voice.notes {
        match note {
            (_, Some(pitch)) => {
                if !is_in_c_major(*pitch) {
                    p += CHROMATIC_NOTE_PENALTY
                        + CONSECUTIVE_CHROMATIC_NOTE_PENALTY * (consecutive_chromatic_notes as f64);
                    consecutive_chromatic_notes += 1;
                } else {
                    consecutive_chromatic_notes = 0;
                }
                if is_in_c_major_pentatonic(*pitch) {
                    p -= PENTATONIC_NOTE_BONUS;
                }
            }
            (duration, None) => {
                p += REST_PENALTY_PER_EIGHTH_NOTE * (*duration as f64) / (EIGHTH_NOTE as f64)
            }
        }
    }
    // penalty of a * (magnitude - 2) ** 2 for jumps greater than 2 semitones,
    // and add an additional penalty for jumps of a tritone
    // and a small penalty for staying on the same note
    // but tolerate medium-sized jumps once in a while
    let mut large_jump_cooldown = 0;
    for (first, second) in voice.notes.iter().filter_map(to_note).adjacent_pairs() {
        let diff = (first.1 as i32) - (second.1 as i32);
        let diff = diff.abs();
        if diff > 2 {
            if large_jump_cooldown > 0 || diff > 7 {
                p += LARGE_JUMP_PENALTY_MULTIPLIER * (diff as f64).powi(2);
            }
            large_jump_cooldown = 3 + (diff - 2) / 3;
        } else if large_jump_cooldown > 0 {
            large_jump_cooldown -= 1;
        }
        if diff % 12 == 6 {
            p += TRITONE_PENALTY;
        }
        if diff == 0 {
            p += SAME_NOTE_PENALTY;
        }
    }
    p
}

pub fn evaluate_tune(tune: &Tune) -> f64 {
    let mut p = 0.0;
    // Add the penalties for each voice
    for voice in &tune.voices {
        p += evaluate_voice(voice);
    }
    // Look at each attack and evaluate the amount of dissonance
    let mut starts: Vec<&[(u32, Option<u8>)]> = tune.voices.iter().map(|v| &v.notes[..]).collect();
    loop {
        // Get the time of the first attack
        let attack_time = starts
            .iter()
            .filter_map(|range| {
                if range.is_empty() {
                    None
                } else {
                    Some(range[0].0)
                }
            })
            .min();
        let attack_time = match attack_time {
            Some(t) => t,
            None => break,
        };
        let mut attack_bitfield: u32 = 0;
        let mut num_attacks = 0;
        let mut doubled_notes = 0;
        for start in starts.iter_mut() {
            while !start.is_empty() && start[0].0 == attack_time {
                match start[0].1 {
                    Some(pitch) => {
                        let pitch_class = pitch % 12;
                        if attack_bitfield & (1 << pitch_class) != 0 {
                            doubled_notes += 1;
                        }
                        attack_bitfield |= 1 << pitch_class;
                        num_attacks += 1;
                    }
                    None => (),
                }
                *start = &start[1..];
            }
        }
        // Penalise dissonance
        if num_attacks != 0 {
            p += DISSONANCE_PENALTY_MULTIPLIER * CHORD_DISSONANCE[attack_bitfield as usize]
                / (num_attacks as f64).powf(1.5);
        }
        // Penalise doubled notes
        if doubled_notes > 1 {
            p += DOUBLED_NOTE_PENALTY * (doubled_notes as f64);
        }
        // Give a bonus to full chords
        if COMPLETES_FULL_CHORDS[attack_bitfield as usize] {
            p -= FULL_CHORD_BONUS;
        }
    }
    p
}

pub fn mutate_tune(s: &mut impl Rng, mut tune: Tune, num_mutations: u32) -> Tune {
    // Select a random voice to mutate
    for _ in 0..num_mutations {
        let (index, voice) = loop {
            let index = s.gen_range(0, tune.voices.len());
            let voice = &mut tune.voices[index];
            let index = s.gen_range(0, voice.notes.len());
            if !voice.pinned {
                break (index, voice);
            }
        };
        let num_notes = s.gen_range(1, 3);
        let num_notes = min(num_notes, voice.notes.len() - index);
        for off in 0..num_notes {
            let index = index + off;
            let note = voice.notes[index];
            let roll = s.gen_range(0, 10);
            let new_note = match note {
                (t, Some(p)) => {
                    if roll > 0 {
                        // 90% chance to change into a different note
                        let offset = s.gen_range(-7, 8);
                        let new_pitch = (p as i32) + offset;
                        let new_pitch = min(127, max(0, new_pitch));
                        (t, Some(new_pitch as u8))
                    } else {
                        // 10% chance to change into a rest
                        (t, None)
                    }
                }
                (t, None) => {
                    if roll > 2 {
                        // 70% chance to keep as a rest
                        (t, None)
                    } else {
                        // 30% chance to change into a note
                        let mut index_offset: isize = 1;
                        let nearest_note = loop {
                            let new_index = ((index as isize) + index_offset) as usize;
                            match voice.notes.get(new_index) {
                                None => break 64u8,
                                Some((_, Some(pitch))) => break *pitch,
                                Some((_, None)) => (),
                            }
                            if index_offset > 0 {
                                index_offset = -index_offset;
                            } else {
                                index_offset = -index_offset + 1;
                            }
                        };
                        let offset = s.gen_range(-7, 8);
                        let new_pitch = (nearest_note as i32) + offset;
                        let new_pitch = min(127, max(0, new_pitch));
                        (t, Some(new_pitch as u8))
                    }
                }
            };
            voice.notes[index] = new_note;
        }
    }
    tune
}

pub fn evolve_tune(s: &mut impl Rng, mut tune: Tune) -> Tune {
    let mut best_score = evaluate_tune(&tune);
    eprintln!("Initial badness: {}", best_score);
    for i in 0..EVOLVE_ITERS {
        let num_mutations = 6 - min(99_999, i) * 6 / 100_000;
        assert!(num_mutations > 0);
        let new_tune = mutate_tune(s, tune.clone(), num_mutations);
        let new_score = evaluate_tune(&new_tune);
        if new_score < best_score {
            eprintln!("At iteration #{}, badness decreased to {}", i, new_score);
            best_score = new_score;
            tune = new_tune;
        }
    }
    tune
}

pub fn evaluate_chord(chord: &Chord) -> f64 {
    let mut p = 0.0;
    // Give penalties for chromatic notes
    for n in chord.notes.iter() {
        if !is_in_c_major(*n) {
            p += CHROMATIC_NOTE_PENALTY;
        }
    }
    // Give penalties for doubled notes
    let mut bitfield: u32 = 0;
    let mut doubled_notes = 0;
    for n in chord.notes.iter() {
        let pitch_class = *n % 12;
        if bitfield & (1 << pitch_class) != 0 {
            doubled_notes += 1;
        }
        bitfield |= 1 << pitch_class;
    }
    if doubled_notes > 1 {
        p += DOUBLED_NOTE_PENALTY * (doubled_notes as f64);
    }
    p
}

pub fn dissonance_of_chord(chord: &Chord) -> f64 {
    let mut bitfield: u32 = 0;
    for n in chord.notes.iter() {
        let pitch_class = *n % 12;
        bitfield |= 1 << pitch_class;
    }
    CHORD_DISSONANCE[bitfield as usize]
}

pub fn evaluate_progression(prog: &ChordProgression) -> f64 {
    let mut p = 0.0;
    // Add the penalties for each voice
    for chord in &prog.chords {
        p += evaluate_chord(chord);
    }
    // Give less penalty to a dissonant chord if it's followed by a consonant one
    for (d1, d2) in prog.chords.iter().map(dissonance_of_chord).adjacent_pairs() {
        if d1 < d2 {
            p += DISSONANCE_PENALTY_MULTIPLIER * d1;
        } else {
            p += DISSONANCE_PENALTY_MULTIPLIER * (d1 + d2) / 2.0;
        }
    }
    // Give an especially high priority on having a consonant final chord
    p += 4.0
        * DISSONANCE_PENALTY_MULTIPLIER
        * prog.chords.last().map(dissonance_of_chord).unwrap_or(0.0);
    // Penalise poor voice-leading
    for (c1, c2) in prog.chords.iter().adjacent_pairs() {
        let mut chord_distance = 0;
        let mut chord_movement = 0;
        for (n1, n2) in c1.notes.iter().zip(c2.notes.iter()) {
            let diff = (*n1 as i32) - (*n2 as i32);
            let diff = diff.abs();
            if diff > 2 {
                chord_distance += diff.pow(2);
            }
            chord_movement += diff;
        }
        if chord_movement < 3 {
            // Penalise chords that are too identical
            p += 15.0;
        } else {
            p += LARGE_JUMP_PENALTY_MULTIPLIER * (chord_distance as f64);
        }
    }
    p
}

pub fn mutate_progression(s: &mut impl Rng, mut prog: ChordProgression) -> ChordProgression {
    let chord = {
        let index = s.gen_range(0, prog.chords.len());
        let chord = &mut prog.chords[index];
        chord
    };
    let note_index = s.gen_range(0, NOTES_PER_CHORD);
    let new_note = loop {
        let k = s.gen_range(36, 60);
        if !chord.notes[..].contains(&k) {
            break k;
        }
    };
    chord.notes[note_index] = new_note;
    chord.notes.sort_unstable();
    prog
}

pub fn evolve_progression(s: &mut impl Rng, mut prog: ChordProgression) -> ChordProgression {
    let mut best_score = evaluate_progression(&prog);
    eprintln!("Initial badness: {}", best_score);
    for i in 0..EVOLVE_ITERS {
        let new_prog = mutate_progression(s, prog.clone());
        let new_score = evaluate_progression(&new_prog);
        if new_score < best_score {
            eprintln!("At iteration #{}, badness decreased to {}", i, new_score);
            best_score = new_score;
            prog = new_prog;
        }
    }
    prog
}
