#![feature(exclusive_range_pattern)]

use ghakuf::messages::*;
use ghakuf::writer::*;
use rand::prelude::*;
use rand::rngs::StdRng;
use std::path;
use std::time::Instant;

pub mod generation;

use generation::draft::*;
use generation::evolution::*;
use generation::to_midi::*;

fn main() {
    {
        let mut write_messages: Vec<Message> =
            vec![set_tempo(TEMPO), set_time_signature(4, 2), end_of_track()];
        let mut s = StdRng::from_entropy();
        let start_time = Instant::now();
        let prog = generate_random_progression(&mut s, 8);
        let prog = evolve_progression(&mut s, prog);
        let tune = generate_tune(&mut s, &[72, 64, 55], &prog);
        let tune = evolve_tune(&mut s, tune);
        println!(
            "Generated a tune in {} us",
            start_time.elapsed().as_micros()
        );
        write_tune(&mut write_messages, &tune);
        let path = path::Path::new("example.mid");
        let mut writer = Writer::new();
        writer.running_status(true);
        for message in &write_messages {
            writer.push(&message);
        }
        let _ = writer.write(&path);
    }
}
